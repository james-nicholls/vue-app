import Vue from 'vue'
import Router from 'vue-router'

import App from '@/app.js'

Vue.use(Router)

const Routes = new Router({
  mode: 'history',
  base: '/',
  routes: [

  ]
})

/* eslint-disable no-new */
new Vue({
  el: '#root',
  router: Routes,
  components: {
    app: App
  },
  render: vc =>
    vc('app')
})
